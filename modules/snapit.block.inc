<?php

/**
 * @file
 * Snapshot functionality on behalf of the block-module (blocks).
 */

/**
 * Implements hook_snapit_info().
 */
function block_snapit_info() {
  return array(
    'block' => array(
      'name' => t('Blocks'),
      'description' => t('Provides functionality to take snapshots of blocks in the Drupal installation.'),
      'build callback' => 'snapit_block_build',
      'compare callback' => 'snapit_block_compare',
      'import callback' => 'snapit_block_import',
      'file path' => drupal_get_path('module', 'snapit') .'/modules',
      'file' => 'snapit.block.inc',
    ),
  );
}

/**
 * Callback function for building snapshot of blocks.
 * @param array $implementer An array cotaining implementer information.
 * @return array An array containing the block-module version and blocks data.
 */
function snapit_block_build($implementer) {
  // Retrieve all blocks data of modules and themes.
  $modules_blocks = _snapit_get_modules_blocks();
  $block_roles = _snapit_get_block_roles();

  // Module delta settings array.
  $settings = array();

  // Retrieve all blocks.
  $data = array();
  $result = db_query('SELECT * FROM {blocks} ORDER BY module, delta');
  while ($record = db_fetch_array($result)) {
    $info = '';
    if ($modules_blocks[$record['module']]) {
      if ($modules_blocks[$record['module']][$record['delta']]) {
        // Set the info field, containing the block name displayed in the back-end.
        $info = $modules_blocks[$record['module']][$record['delta']]['info'];
      }
    }

    // At this moment only the title of a block is saved in the snapshot.
    // It's possible to add more settings of a block in the near future.
    if (!empty($info)) {
      $settings[$record['module']][$record['delta']]['info'] = $info;
    }
    // Set the configure field in the snapshot.
    if (!isset($data[$record['module']][$record['delta']]['configure'])) {
      $data[$record['module'] . '_' . $record['delta']]['configure'] = $settings[$record['module']][$record['delta']];
    }

    // Set block roles, if are set for the module-delta.
    if (isset($block_roles[$record['module']][$record['delta']]) && !isset($data[$record['module']][$record['delta']]['block_roles'])) {
      $data[$record['module'] . '_' . $record['delta']]['roles'] = $block_roles[$record['module']][$record['delta']];
    }

    // We don't need the bid for snapshot, it can differ on different Drupal
    // installations.
    unset($record['bid']);
    $data[$record['module'] . '_' . $record['delta']][$record['theme']] = $record;
  }

  $system_info = snapit_module_info('block');
  return array(
    'version' => $system_info->info['version'],
    'data' => $data,
  );
}

/**
 * Callback function for comparing blocks.
 *
 * @param array $left Current/source snapshot data.
 * @param array $right New/import snapshot data.
 * @param array $implementer Implemneter info.
 * @return array $return Array containing values and differences of blocks.
 */
function snapit_block_compare($left, $right, $implementer) {
  $return = snapit_compare($left, $right, $implementer);

  $modules_delta = array_keys($return);
  foreach ($modules_delta as $module) {
    // Set or re-set the (real) name of the current block.
    $return[$module]['#name'] = (!empty($return[$module]['#left']['configure']['info']) ? $return[$module]['#left']['configure']['info'] : $return[$module]['#right']['configure']['info']);
  }
  return $return;
}

/**
 * Import callback for block(s).
 * note: Added blocks by/with the block module are not added.
 * @param array $data Array containing all the blocks-data to be
 *   imported/updated.
 * @return bool TRUE if update succeeded; FALSE if no updates were made or
 *   something went wrong.
 */
function snapit_block_import($data) {
  if (is_array($data)) {
    $modules_delta = array_keys($data);
    foreach ($modules_delta as $module) {

      // Get the keys of the current block-delta.
      $delta_keys = array_unique( array_merge( (is_array($data[$module]['#left']) ? array_keys($data[$module]['#left']) : array()), (is_array($data[$module]['#right']) ? array_keys($data[$module]['#right']) : array()) ));
      unset($delta_keys[array_search('configure', $delta_keys)]);
      unset($delta_keys[array_search('roles', $delta_keys)]);

      // Below some general puropse block parameters are imported/updated.
      if ($data[$module]['#type'] == SNAPIT_MODIFIED || $data[$module]['#type'] == SNAPIT_ADDED) {
        // Used for selecting some additional needed block information.
        $select_key = $delta_keys[count($delta_keys)-1];

        // If modify, titles differ and it's a block created by the
        // block-module, change the title.
        // note: This functionality is added, but is't a bit optional and can
        // be disabled if boxes functionality is active.
        if ($data[$module]['#left']['configure']['info'] != $data[$module]['#right']['configure']['info']
           && $data[$module]['#right'][$delta_keys[$select_key]]['module'] == 'block') {
          db_query("UPDATE {boxes} SET info = '%s' WHERE bid = %d", $data[$module]['#right']['configure']['info'], $data[$module]['#right'][$select_key]['delta']);
        }

        // Update Roles.
        if (isset($data[$module]['#left']['roles']) || isset($data[$module]['#right']['roles'])) {
          db_query("DELETE FROM {blocks_roles} WHERE module = '%s' AND delta = '%s'", $data[$module]['#right'][$select_key]['module'], $data[$module]['#right'][$select_key]['delta']);
          if (is_array($data[$module]['#right']['roles'])) {
            foreach ($data[$module]['#right']['roles'] as $rid) {
              db_query("INSERT INTO {blocks_roles} (rid, module, delta) VALUES (%d, '%s', '%s')", $rid, $data[$module]['#right'][$select_key]['module'], $data[$module]['#right'][$select_key]['delta']);
            }
          }
        }
      }

      foreach ($delta_keys as $theme) {
        if ($data[$module]['#type'] == SNAPIT_MODIFIED || $data[$module]['#type'] == SNAPIT_ADDED) {
          if ($data[$module]['#type'] == SNAPIT_ADDED) {
            if ($data[$module]['#right'][$theme]['module'] != 'block') {
              db_query("INSERT INTO {blocks} (visibility, pages, custom, title, module, theme, status, weight, delta, cache) VALUES(%d, '%s', %d, '%s', '%s', '%s', %d, %d, '%s', %d)", $data[$module]['#right'][$theme]['visibility'], trim($data[$module]['#right'][$theme]['pages']), $data[$module]['#right'][$theme]['custom'], $data[$module]['#right'][$theme]['title'], $data[$module]['#right'][$theme]['module'], $theme, $data[$module]['#right'][$theme]['status'], $data[$module]['#right'][$theme]['weight'], $data[$module]['#right'][$theme]['delta'], BLOCK_NO_CACHE);
            }
          }
          else {
            // If some theming/templating changed, some blocks could have to be
            // added or deleted and otherwise just an update.
            if (isset($data[$module]['#left'][$theme]) && !isset($data[$module]['#right'][$theme])) {
              // Del
              db_query("DELETE FROM {blocks} WHERE module = '%s' AND delta = '%s' AND theme = '%s'", $data[$module]['#left'][$theme]['module'], $data[$module]['#left'][$theme]['delta'], $theme);
            }
            elseif (!isset($data[$module]['#left'][$theme]) && isset($data[$module]['#right'][$theme])) {
              // Add
              db_query("INSERT INTO {blocks} (visibility, pages, custom, title, module, theme, status, weight, delta, cache) VALUES(%d, '%s', %d, '%s', '%s', '%s', %d, %d, '%s', %d)", $data[$module]['#right'][$theme]['visibility'], trim($data[$module]['#right'][$theme]['pages']), $data[$module]['#right'][$theme]['custom'], $data[$module]['#right'][$theme]['title'], $data[$module]['#right'][$theme]['module'], $theme, $data[$module]['#right'][$theme]['status'], $data[$module]['#right'][$theme]['weight'], $data[$module]['#right'][$theme]['delta'], BLOCK_NO_CACHE);
            }
            else {
              // Update
              db_query("UPDATE {blocks} SET status = %d, weight = %d, region = '%s', throttle = %d, title = '%s' WHERE module = '%s' AND delta = '%s' AND theme = '%s'", $data[$module]['#right'][$theme]['status'], $data[$module]['#right'][$theme]['weight'], $data[$module]['#right'][$theme]['region'], (isset($data[$module]['#right'][$theme]['throttle']) ? $data[$module]['#right'][$theme]['throttle'] : 0), $data[$module]['#right'][$theme]['title'], $data[$module]['#right'][$theme]['module'], $data[$module]['#right'][$theme]['delta'], $data[$module]['#right'][$theme]['theme']);
            }
          }
        }
        elseif ($data[$module]['#type'] == SNAPIT_REMOVED) {
          if ($data[$module]['#right'][$theme]['module'] == 'block') {
            db_query('DELETE FROM {boxes} WHERE bid = %d', $data[$module]['#left'][$theme]['delta']);
          }
          db_query("DELETE FROM {blocks_roles} WHERE module = '%s' AND delta = '%s'", $data[$module]['#left'][$theme]['module'], $data[$module]['#left'][$theme]['delta']);
          db_query("DELETE FROM {blocks} WHERE module = '%s' AND delta = '%s'", $data[$module]['#left'][$theme]['module'], $data[$module]['#left'][$theme]['delta']);
        }
      }
    }
    return TRUE;
  }
  return FALSE;
}

/**
 * Helper function for requesting all blocks info-field/name.
 * @return array $modules_blocks Contains additional information per delta of a
 *   block.
 */
function _snapit_get_modules_blocks() {
  $modules = module_list();
  foreach ($modules as $module) {
    $modules_blocks[$module] = module_invoke($module, 'block', 'list');
  }
  return $modules_blocks;
}

/**
 * Helper function for requesting all blocks info-field/name.
 * @return array $modules_blocks Contains additional information per delta of a
 *   block.
 */
function _snapit_get_block_roles() {
  $block_roles = array();

  $result = db_query('SELECT * FROM {blocks_roles} ORDER BY module, delta');
  while ($record = db_fetch_array($result)) {
    $block_roles[$record['module']][$record['delta']][] = $record['rid'];
  }
  return $block_roles;
}