<?php

/**
 * @file
 * Snapshot functionality for user (permissions, rules).
 */

/**
 * Implements hook_snapit_info().
 */
function user_snapit_info() {
  return array(
    'roles_permissions' => array(
      'name' => t('Roles'),
      'description' => t('Provides functionality to take snapshots of user roles/permissions in the Drupal installation.'),
      'build callback' => 'snapit_roles_permissions_build',
      'compare callback' => 'snapit_roles_permissions_compare',
      'details callback' => 'snapit_roles_permissions_detail_compare',
      'import callback' => 'snapit_roles_permissions_import',
      'file path' => drupal_get_path('module', 'snapit') .'/modules',
      'file' => 'snapit.user.inc',
    ),
    'access_rules' => array(
      'name' => t('Access rules'),
      'description' => t('Provides functionality to take snapshots of user access rules in the Drupal installation.'),
      'build callback' => 'snapit_access_rules_build',
      'compare callback' => 'snapit_access_rules_compare',
      'import callback' => 'snapit_access_rules_import',
      'file path' => drupal_get_path('module', 'snapit') .'/modules',
      'file' => 'snapit.user.inc',
    ),
  );
}

/**
 * Callback function for building snapshot of user-roles/permissions.
 * @param array $implementer An array cotaining implementer information.
 * @return array An array containing the user version and user-roles/permissions-data.
 */
function snapit_roles_permissions_build($implementer) {
  // Retrieve all user roles/permissions.
  $data = array();
  $result = db_query("SELECT r.name, r.rid, p.perm FROM {role} as r LEFT JOIN {permission} as p ON r.rid = p.rid ORDER BY rid");
  while ($record = db_fetch_array($result)) {
    // Build-up a certain set of needed values.

    // Sort the permisions.
    $perm = explode(', ', $record['perm']);
    sort($perm);
    $record['perm'] = implode(', ', $perm);

    $data[$record['rid']]['perm'] = $record['perm'];
    $data[$record['rid']]['rid'] = $record['rid'];
    $data[$record['rid']]['name'] = $record['name'];
  }

  $system_info = snapit_module_info('user');
  return array(
    'version' => $system_info->info['version'],
    'data' => $data,
  );
}

/**
 * Callback function for comparing user roles/permissions
 *
 * @param array $left Current/source snapshot data.
 * @param array $right New/import snapshot data.
 * @param array $implementer Implemneter info.
 * @return array $return Array containing values and differences of user
 *   roles/permisions.
 */
function snapit_roles_permissions_compare($left, $right, $implementer) {
  $return = snapit_compare($left, $right, $implementer);
  $keys = array_keys($return);
  foreach ($keys as $key) {
    $return[$key]['#name'] = (!empty($return[$key]['#left']['name']) ? $return[$key]['#left']['name'] : $return[$key]['#right']['name']);
    if (!empty($return[$key]['#right']['perm'])) {
      $return[$key]['#right']['perm'] = str_replace(", ", "\n", $return[$key]['#right']['perm']);
    }
    if (!empty($return[$key]['#left']['perm'])) {
      $return[$key]['#left']['perm'] = str_replace(", ", "\n", $return[$key]['#left']['perm']);
    }
  }
  return $return;
}

/**
 * Import callback for user-roles/permissions.
 * @param array $data Array containing all the user-roles/permissions and
 *   values to be imported/updated.
 * @return bool TRUE if update succeeded; FALSE if no updates were made or
 *   something went wrong.
 */
function snapit_roles_permissions_import($data) {
  if (is_array($data)) {
    foreach ($data as $role => $values) {
      // Insert or Update role.
      $result_role = _snapit_change_user_role($role, $values['#right']['name'], $values['#type']);

      // If a new record is INSERTED a new record_id is returned, otherwise
      // we use the provided.

      // Insert or Update permissions.
      $result_perm = _snapit_change_user_permissions($role, $values['#right']['perm'], $values['#type']);

      if ($values['#type'] == SNAPSHOT_REMOVED) {
        // If are set REMOVE users_roles also.
        db_query('DELETE FROM {users_roles} WHERE rid = %d', $rid);
      }
    }

    // Clear the cached pages
    cache_clear_all();
    return TRUE;
  }
  return FALSE;
}

/**
 * Some details of the implenter are checked.
 * If an error or possible issue is found a warning or error will be displayed.
 * If it's realy an blocking error, the function will return TRUE, which
 * results in the import button shall not be displayed.
 *
 * @param array $data Snapshot data (imported)
 * @param array $implementer Implementer info
 * @return bool TRUE if blocking errors are found.
 */
function snapit_roles_permissions_detail_compare($data, $implementer) {
  $warning = FALSE;
  if (is_array($data) && !empty($data)) {
    foreach ($data as $role => $values) {
      if (!empty($values['#right'])) {
        if (empty($values['#right']['perm'])) {
          $warning = t('No permisions set for role %role.', array('%role' => $role));
        }
      }
      if ($warning) {
        drupal_set_message($warning, 'warning');
      }
    }
  }
}

/**
 * Change the role
 * @param int $rid, Role id (relation with the role table)
 * @param string $name, Name of the role.
 * @param int $action, Remove (0), Add (1), Modify (2)
 */
function _snapit_change_user_role($rid, $name, $action) {
  $change_result = FALSE;
  switch ($action) {
    case SNAPIT_ADDED:
      // We force the rid, so they stay the same in the different enviroments/versions of the site.
      if (db_query("INSERT INTO {role} (rid, name) VALUES (%d, '%s')", $rid, $name)) {
        $change_result = db_last_insert_id('role', 'rid');
      }
      break;

    case SNAPIT_MODIFIED:
      if (db_query("UPDATE {role} SET name = '%s' WHERE rid = %d", $name, $rid)) {
        $change_result = TRUE;
      }
      break;

    case SNAPIT_REMOVED:
      if (db_query("DELETE FROM {role} WHERE rid = %d", $rid)) {
        $change_result = TRUE;
      }
      break;
  }
  return $change_result;
}

/**
 * Change the permissions for a certain role.
 * @param int $rid, Role id (relation with the role table)
 * @param array $permissions, Contains the permissions
 * @param int $action, Remove (0), Add (1), Modify (2)
 */
function _snapit_change_user_permissions($rid, $permissions, $action) {
  $change_result = FALSE;

  // The old record will always be deleted
  db_query('DELETE FROM {permission} WHERE rid = %d', $rid);

  if ($action == SNAPIT_ADDED || $action == SNAPIT_MODIFIED) {
    $permissions = (empty($permissions) ? NULL : $permissions);
    db_query("INSERT INTO {permission} (rid, perm) VALUES (%d, '%s')", $rid, str_replace("\n", ", ", $permissions));
    $change_result = db_last_insert_id('role', 'rid');
  }
  elseif ($acion == SNAPIT_REMOVED) {
    // The record is already deleted so we return TRUE.
    $change_result = TRUE;
  }
  return $change_result;
}

/**
 * Callback function for building snapshot of user-access-rules.
 * @param array $implementer An array cotaining implementer information.
 * @return array An array containing the user version and user-access-rules
 *   data.
 */
function snapit_access_rules_build($implementer) {
  // Retrieve all user access rules
  $data = array();
  $result = db_query("SELECT * FROM {access} ORDER BY aid, type, status");
  while ($record = db_fetch_array($result)) {
    // Build up of the access rules, because they don't have no unique name and we don't
    // want to display just a unique number, we create a unique key.
    $data[$record['aid']] = $record;
  }

  $system_info = snapit_module_info('user');
  return array(
    'version' => $system_info->info['version'],
    'data' => $data,
  );
}
/**
 * Callback function for comparing user access rules.
 *
 * @todo Check if check_plain() is properly implemented.
 *
 * @param array $left Current/source snapshot data.
 * @param array $right New/import snapshot data.
 * @param array $implementer Implemneter info.
 * @return array $return Array containing values and differences of user access
 *   rules.
 */
function snapit_access_rules_compare($left, $right, $implementer) {
  $return = snapit_compare($left, $right, $implementer);
  $keys = array_keys($return);
  foreach ($keys as $key) {
    $return[$key]['#name'] = check_plain((!empty($return[$key]['#left']) ? ($return[$key]['#left']['status'] ? t('Allow') : t('Deny'))  . ' - ' .  $return[$key]['#left']['type'] . ' - ' . $return[$key]['#left']['mask'] : $return[$key]['#right']['mask']));
  }
  return $return;
}

/**
 * Import callback for user-access-rules.
 * @param array $data Array containing all the user-roles/permissions and
 *   values to be imported/updated.
 * @return bool TRUE if update succeeded; FALSE if no updates were made or
 *   something went wrong.
 */
function snapit_access_rules_import($data) {
  if (is_array($data)) {
    foreach ($data as $aid => $values) {
      if ($values['#type'] == SNAPIT_MODIFIED) {
        db_query("UPDATE {access} SET mask = '%s', type = '%s', status = '%s' WHERE aid = %d", $values['#right']['mask'], $values['#right']['type'], $values['#right']['status'], $values['#right']['aid']);
      }
      elseif ($values['#type'] == SNAPIT_ADDED) {
        // We force the aid, so they stay the same in the different
        // enviroments/versions of the site.
        db_query("INSERT INTO {access} (aid, mask, type, status) VALUES (%d, '%s', '%s', %d)", $values['#right']['aid'], $values['#right']['mask'], $values['#right']['type'], $values['#right']['status']);
      }
      elseif ($values['#type'] == SNAPIT_REMOVED) {
        db_query('DELETE FROM {access} WHERE aid = %d', $aid);
      }
    }
    return TRUE;
  }
  return FALSE;
}