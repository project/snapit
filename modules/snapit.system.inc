<?php

/**
 * @file
 * File containing snapit functionality on behalf of system:
 * - Modules
 * - Themes
 * - Settings
 */

/**
 * Implements hook_snapit_info().
 */
function system_snapit_info() {
  $path = drupal_get_path('module', 'snapit') .'/modules';

  return array(
    'modules' => array(
      'name' => t('Modules'),
      'description' => t('Provides functionality to take snapshots of the modules within the Drupal installation.'),
      'build callback' => 'snapit_system_build',
      'compare callback' => 'snapit_compare',
      'details callback' => 'snapit_system_detail_compare',
      'import callback' => 'snapit_modules_import',
      'file' => 'snapit.system.inc',
      'file path' => $path,
      'weight' => -50,
    ),
     'variables' => array(
      'name' => t('Settings'),
      'description' => t('Provides functionality to take snapshots of the configuration options (variables) in the Drupal installation.'),
      'build callback' => 'snapit_variable_build',
      'compare callback' => 'snapit_compare',
      'import callback' => 'snapit_variable_import',
      'file' => 'snapit.system.inc',
      'file path' => $path,

      // Because the values in the table variables are serialized.
      'serialized' => TRUE,
      'weight' => -45,
    ),
    'themes' => array(
      'name' => t('Themes'),
      'description' => t('Provides functionality to take snapshots of themes within the Drupal installation.'),
      'build callback' => 'snapit_system_build',
      'compare callback' => 'snapit_compare',
      'details callback' => 'snapit_system_detail_compare',
      'import callback' => 'snapit_themes_import',
      'file' => 'snapit.system.inc',
      'file path' => $path,
      'weight' => -40,
    ),
  );
}

/**
 * Implements hook_snapit_compare_alter().
 */
function system_snapit_compare_alter(&$diff, $implementer) {
  if ($implementer['module'] == 'system' && $implementer['delta'] == 'variables') {
    $exclude = array(
      'cron_last',
      'cron_semaphore',
      'css_js_query_string',
      'drupal_private_key',
      'install_time',
      'javascript_parsed',
      'update_last_check',
    );
    foreach ($exclude as $key) {
      unset($diff[$key]);
    }
  }
}

/**
 * Snapit build callback for modules and themes.
 *
 * @param array $implementer An array cotaining implementer information.
 * @return array An array containing the system-version and system-modules or
 *    system-themes data/information.
 */
function snapit_system_build($implementer) {
  $data = array();

  // Get the values for the correct system-delta type.
  $system_delta = _snapit_system_delta_info();

  // Select some data from the system table. We need all modules/themes,
  // otherwise we can't detect if a module/theme is disabled or non-existing.
  $result = db_query("SELECT name, status , schema_version, weight, info
                      FROM {system}
                      WHERE type = '%s'
                      ORDER BY weight ASC, filename ASC",
                      $system_delta[$implementer['delta']]);

  while ($record = db_fetch_array($result)) {

    // Unserialize the info field.
    $info = @unserialize($record['info']);

    // Set a certain set of variables.
    if (!empty($info['project'])) {
      $data[$record['name']]['project'] = $info['project'];
    }
    $data[$record['name']]['version'] = $info['version'];
    $data[$record['name']]['status'] = $record['status'];
    $data[$record['name']]['weight'] = $record['weight'];
  }

  $system_info = snapit_module_info('system');

  return array(
    'version' => $system_info->info['version'],
    'data' => $data,
  );
}

/**
 * Some details of the specified implementer are checked.
 * If an error or possible issue is found a warning or error will be displayed.
 * If it's realy an blocking error, the function will return TRUE, which
 * results in the import button shall not be displayed.
 *
 * @param array $data Snapshot data (imported)
 * @param array $implementer Implementer info
 * @return bool TRUE if blocking errors are found.
 */
function snapit_system_detail_compare($data, $implementer) {
  $system_delta = _snapit_system_delta_info();
  // If blocking gets TRUE an import won't become possible.
  $blocking = FALSE;
  if (is_array($data) && !empty($data)) {
    include_once './includes/install.inc';

    foreach ($data as $delta => $values) {
      if ($values['#right']['status'] == 1) {
        $path = drupal_get_path($system_delta[$implementer['delta']], $delta);
        if (empty($path) && ($system_delta[$implementer['delta']] == 'module' && module_exists($delta))) {
          // BLOCKING: Module not active and not installed
          $warnings[] = t('@type not found within this Drupal installation.', array('@type' => ucfirst($system_delta[$implementer['delta']])));
          $warnings[] = t('Version: %version', array('%version' => $values['#right']['version']));

          // To Block the import process.
          $blocking = TRUE;
        }
        elseif (!drupal_check_module($delta) && $system_delta[$implementer['delta']] == 'module') {
          $blocking = TRUE;
        }
        else {
          if (!empty($values['#right'])) {
            $delta_info = ($system_delta[$implementer['delta']] == 'module' ? snapit_module_info($delta) : snapit_theme_info($delta));
            if ($delta_info->info['version'] != $values['#right']['version']) {
              $warnings[] = t("Versions don't match (current: @current_version needed: @needed_version).", array('@current_version' => $delta_info->info['version'], '@needed_version' => $values['#right']['version']));
            }
          }
        }
        // If warning message is set, set Drupal message.
        if (!empty($warnings)) {
          // Format message.
          $msg = ucfirst($system_delta[$implementer['delta']]) . ': ' . ucfirst($delta);
          // Set all the warnings in the msg.
          foreach ($warnings as $warning) {
            $msg .= '<br/>'.$warning;
          }

          // Set url if available.
          if (isset($values['#right']['project'])) {
            $msg .= '<br/>' . t('Project: !project_url', array('!project_url' => l(ucfirst($values['#right']['project']), 'http://drupal.org/project/' . $values['#right']['project'])));
          }
          drupal_set_message($msg, ($blocking ? 'error' : 'warning'));
        }
      }
    }
  }
  return $blocking;
}

/**
 * Import callback for modules.
 *
 * @param array $data Array containing all the system-modules and values to be
 *   imported/updated.
 * @return bool TRUE if import is succesfull.
 */
function snapit_modules_import($data) {
  if (!empty($data)) {
    include_once './includes/install.inc';

    $new_modules = array();
    $enable_modules = array();
    $disable_modules = array();

    // Prepare the import/update.
    foreach ($data as $module => $values) {
      if (($values['#type'] == SNAPIT_ADDED || $values['#type'] == SNAPIT_MODIFIED) && $values['#right']['status'] == 1) {
        if (drupal_get_installed_schema_version($module) == SCHEMA_UNINSTALLED) {
          $new_modules[] = $module;
        }
        else {
          $enable_modules[] = $module;
        }
      }
      elseif ($values['#type'] == SNAPIT_REMOVED || ($values['#type'] == SNAPIT_MODIFIED && $values['#left']['status'] == 1 && $values['#right']['status'] == 0)) {
        $disable_modules[] = $module;
      }
    }

    $old_module_list = module_list();

    // Enable, disable or install modules.
    if (!empty($enable_modules)) {
      module_enable($enable_modules);
    }
    if (!empty($disable_modules)) {
      module_disable($disable_modules);
    }
    if (!empty($new_modules)) {
      drupal_install_modules($new_modules);
    }

    $current_module_list = module_list(TRUE, FALSE);
    if ($old_module_list != $current_module_list) {
      watchdog('snapit', 'Succesfully imported/updated modules');
    }

    drupal_rebuild_theme_registry();
    node_types_rebuild();
    menu_rebuild();
    cache_clear_all('schema', 'cache');

    // Notify locale module about module changes, so translations can be
    // imported. This might start a batch, and only return to the redirect
    // path after that.
    module_invoke('locale', 'system_update', $new_modules);

    // Synchronize to catch any actions that were added or removed.
    actions_synchronize();

    return TRUE;
  }
  return FALSE;
}

/**
 * Callback function for building snapshot.
 *
 * @param array $implementer An array containing implementer information.
 * @return array An array containing the system-variables version and
 *   variables-data.
 */
function snapit_variable_build($implementer) {

  // Select all data from the variable table.
  $data = array();
  $result = db_query("SELECT * FROM {variable} ORDER BY name");
  while ($record = db_fetch_array($result)) {
    $data[$record['name']] = $record['value'];
  }

  $system_info = snapit_module_info('system');

  return array(
    'version' => $system_info->info['version'],
    'data' => $data,
  );
}

/**
 * Import callback for variables.
 *
 * @param array $data Array containing all the system-variables and values to
 *   be imported/updated.
 * @return bool TRUE if import is succesfull.
 */
function snapit_variable_import($data) {
  if (!empty($data)) {
    foreach ($data as $key => $values) {
      if ($values['#type'] == SNAPIT_ADDED || $values['#type'] == SNAPIT_MODIFIED) {
        variable_set($values['#name'], $values['#right']);
      }
      elseif ($values['#type'] == SNAPIT_REMOVED) {
        variable_del($values['#name']);
      }
    }

    // Clear theme cache.
    list_themes(TRUE);
    return TRUE;
  }
  return FALSE;
}

/**
 * Import callback for themes.
 * @param array $data
 *   Array containing all the system-variables and values to be
 *   imported/updated.
 * @return bool TRUE if import is succesfull.
 */
function snapit_themes_import($data) {
  if (!empty($data)) {
    drupal_clear_css_cache();

    // Store list of previously enabled themes and disable all themes.
    $old_theme_list = $new_theme_list = array();
    foreach (list_themes() as $theme) {
      if ($theme->status) {
        $old_theme_list[] = $theme->name;
      }
    }

    foreach ($data as $theme => $values) {
      if ($values['#right']['status'] == 1) {
        // Enable or add.
        system_initialize_theme_blocks($theme);
        $new_theme_list[] = $theme;
      }
      db_query("UPDATE {system} SET status = %d WHERE type = 'theme' and name = '%s'", $values['#right']['status'], $theme);
    }

    list_themes(TRUE);
    menu_rebuild();
    drupal_rebuild_theme_registry();

    // Notify locale module about new themes being enabled, so translations can
    // be imported. This might start a batch, and only return to the redirect
    // path after that.
    module_invoke('locale', 'system_update', array_diff($new_theme_list, $old_theme_list));
    return TRUE;
  }

  return FALSE;
}

/**
 * General purpose data used by system-modules and system-themes functionality.
 * For example used in SQL-query's to determine the type.
 *
 * @return array
 *   Returns an array containing module-delta as key and singular naming as
 *   value.
*/
function _snapit_system_delta_info() {
  return array(
    'modules' => 'module',
    'themes' => 'theme',
  );
}