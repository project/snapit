<?php

/**
 * @file
 * Snapshot functionality for views.
 */

/**
 * Implements hook_snapit_info().
 */
function views_snapit_info() {
  return array(
    'views' => array(
      'name' => t('Views'),
      'description' => t('Provides functionality to take snapshots of views in the Drupal installation.'),
      'build callback' => 'snapit_views_build',
      'compare callback' => 'snapit_compare',
      'details callback' => 'snapit_views_detail_compare',
      'import callback' => 'snapit_views_import',
      'file path' => drupal_get_path('module', 'snapit') .'/modules',
      'file' => 'snapit.views.inc',
      'serialized' => TRUE,
    ),
  );
}

/**
 * Callback function for building snapshot of Views.
 * @param array $implementer An array cotaining implementer information.
 * @return array An array containing the views version and views-data.
 */
function snapit_views_build($implementer) {
  $data = array();
  if (!views_include('view')) {
    return $data;
  }
  // Initialize a views object.
  $view = new view();
  // Load all the views in this Drupal installation. An array with view-objects
  // is returned.
  $views = $view->load_views();
  // Get the view-names (keys).
  $keys = array_keys($views);

  foreach ($keys as $key) {
    // We need to exclude the vid, this can differ in the differnent Drupal
    // installations. The unique key per view is the view-name.
    unset($views[$key]->vid);
    $data[$key] = serialize($views[$key]);
  }

  $system_info = snapit_module_info('views');
  return array(
    'version' => $system_info->info['version'],
    'data' => $data,
  );
}

/**
 * Some details of the implenter are checked.
 * If an error or possible issue is found a warning or error will be displayed.
 * If it's realy an blocking error, the function will return TRUE, which
 * results in the import button shall not be displayed.
 *
 * @param array $data Snapshot data (imported)
 * @return bool TRUE if blocking errors are found.
 */
function snapit_views_detail_compare($data) {
  $blocking = FALSE;
  if (is_array($data) && !empty($data)) {
    foreach ($data as $key => $values) {
      if (!empty($values['#right'])) {
        $import_view = $values['#right'];
        // Is it a view-object?
        if (is_object($import_view)) {
          // Note: The validation is slow.
          if (!$error = $import_view->validate()) {
            // If there are errors they're returned as strings in an array.
            $error_count = count($error);
            for ($i=0;$i<$error_count;$i++) {
              drupal_set_message($error[$i], 'error');
            }
            $warning = '<br/>' . t('View not valid.');
            $blocking = TRUE;
          }
          // Check where there is't not an old views import.
          if (isset($import_view->url) || isset($import_view->page) || isset($import_view->block)) {
            views_include('convert');
            $import_view = views1_import($import_view);
            drupal_set_message(t('You are importing a view created in Views version 1. You may need to adjust some parameters to work correctly in version 2.'), 'warning');
          }
        }
        else {
          $warning = t("View %view isn't a view-object.", array('%view' => $key));
          $blocking = TRUE;
        }
      }
      if (!empty($warning)) {
        $warning = t('View: @view', array('@view' => ucfirst($key)))
                      . $warning;
        drupal_set_message($warning, ($blocking ? 'error' : 'warning'));
      }
    }
  }
  return $blocking;
}

/**
 * Import callback for view-imports
 *
 * @param array $data Array containing all the views and their values to be imported/updated.
 * @return bool TRUE if import is succesfull.
 */
function snapit_views_import($data) {
  if (!empty($data) && views_include('view')) {
    foreach ($data as $key => $values) {
      // Set the view object.
      $import_view = $values['#right'];

      if ($values['#type'] == SNAPIT_ADDED) {
        $import_view->vid = 'new';
        $import_view->save();
      }
      elseif ($values['#type'] == SNAPIT_MODIFIED) {
        // Get the current vid and set it for the import data.
        $temp_view = views_get_view($import_view->name);
        $import_view->vid = $temp_view->vid;

        $import_view->save();
      }
      elseif ($values['#type'] == SNAPIT_REMOVED) {
        // The right value isn't available here.
        $import_view = $values['#left'];
        $import_view->delete();
      }
    }

    // Make sure menu items get rebuilt as neces
    menu_rebuild();
    // Clear the views cache.
    cache_clear_all('*', 'cache_views');
    // Unset the view object.
    unset($import_view);
    return TRUE;
  }
  return FALSE;
}