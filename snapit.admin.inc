<?php

/**
 * @file
 * Module file containing administrational functionality for the snapshot
 * module.
 */

/**
 * Menu callback for the default admin overview. Displays a list of all created
 * snapshots.
 *
 * return string $output HTML
 */
function snapit_admin_overview() {
  module_load_include('inc', 'snapit');

  $header = array(
    array('data' => t('Name'), 'field' => 's.name'),
    t('Description'),
    array('data' => t('Size'), 'field' => '_size_total'),
    array('data' => t('Created'), 'field' => 's.created', 'sort' => 'desc'),
    array('data' => t('Account'), 'field' => 'u.name'),
  );

  // The overview display limit.
  $limit = intval(variable_get('snapit_overview_limit', 10));

  $implementers = _snapit_implementers();
  $query = _snapit_admin_overview_query($implementers);
  $sql = $query['sql'] . tablesort_sql($header);

  $result = pager_query($sql, $limit, 0, $query['count']);
  $rows = array();

  while ($record = db_fetch_object($result)) {

    // Build the table rows.
    $rows[] = array(
      l($record->name, 'admin/build/snapit/'. $record->sid),
      check_plain($record->description),
      format_size($record->_size_total),
      format_date($record->created, 'medium'),
      theme('username', (object)array('name' => $record->account_name, 'uid' => $record->uid)),
    );
  }

  if (count($rows) == 0) {
    $rows[] = array(array('data' => t('No snapshots have been taken so far.'), 'colspan' => count($header)));
  }

  $output = theme('table', $header, $rows);
  $output .= theme('pager', array(), $limit, 0);

  return $output;
}

/**
 * Menu callback for the snapit build page for adding a snapshot.
 *
 * @param array $form_state Form state information.
 * @return array  $form An associative array containing the structure of the snapshot-build/add form.
 */
function snapit_admin_build_form(&$form_state) {

  $form['name'] = array(
    '#type' => 'textfield',
    '#title' => t('Snapshot name'),
    '#description' => t('Please provide a name for your snapshot.'),
    '#required' => TRUE,
    '#maxlength' => 100,
  );

  $form['description']= array(
    '#type' => 'textarea',
    '#title' => t('Description'),
    '#required' => TRUE,
    '#description' => t('You can enter a description here, this should include information about the reason of making the snapshot.'),
  );

  module_load_include('inc', 'snapit');
  $implementers = _snapit_implementers();
  $options = array();
  foreach ($implementers as $key => $implementer) {
    $options[$key] = $implementer['name'];
  }

  $form['include'] = array(
    '#type' => 'item',
    '#title' => t('Snapshot contains'),
    '#value' => theme('item_list', $options),
  );

  $form['buttons'] = array(
    '#prefix' => '<div class="container-inline">',
    '#suffix' => '</div>',
  );

  $form['buttons']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Take snapshot'),
  );

  $form['buttons']['cancel'] = array(
    '#value' => l(t('Cancel'), 'admin/build/snapit'),
  );

  return $form;
}

/**
 * Submit handler function for the snapit_admin_build_form().
 */
function snapit_admin_build_form_submit($form, &$form_state) {
  module_load_include('api.inc', 'snapit');

  $snapshot = $form_state['values'];
  unset($snapshot['op'], $snapshot['submit'], $snapshot['form_build_id'], $snapshot['form_token'], $snapshot['form_id']);

  if ($snapshot = snapit_save($snapshot)) {
    drupal_set_message(t('Successfully created snapshot %name.', array('%name' => $snapshot->name)));
    $form_state['redirect'] = 'admin/build/snapit';
  }
  else {
    drupal_set_message(t('An error occurred while saving the snapshot. Please check the error logs.'), 'error');
  }
}

/**
 * Menu callback for import form.
 *
 * @param array $form_state Form state information.
 * @return array $form An associative array containing the structure of the form import.
 */
function snapit_admin_import(&$form_state) {
  module_load_include('inc', 'snapit');
  module_load_include('api.inc', 'snapit');

  $step = (isset($form_state['values']) ? (int)$form_state['storage']['step'] : 0);
  $step++;

  // Submit button, will default been shown.
  $show_button = TRUE;

  // Current step in import flow.
  $form_state['storage']['step'] = $step;

  if ($step == 1) {

    // Build the page title.
    $title = t('Import') . ' - ' . t('Upload snapshot') . ' - ' . t('step @step - 3', array('@step' => $step));
    drupal_set_title($title);
    $form['#title'] = $title;

    // Provide the upload field.
    $form['import_upload'] = array(
      '#type' => 'file',
      '#title' => t('Import file'),
      '#description' => t('Please browse and select a snapshot file for importing, this file should have the extension: .snap'),
    );
  }
  elseif ($step == 2) {

    // Build the page title.
    $title = t('Import') . ' - ' . t('Compare snapshot') . ' - ' . t('step @step - 3', array('@step' => $step));
    drupal_set_title($title);
    $form['#title'] = $title;

    drupal_add_css(drupal_get_path('module', 'snapit') .'/snapit.css');

    // Load & prepare import (file) data.
    if (empty($form_state['storage']['import_file_location'])) {
      $file_object = snapit_import_save_file();
    }
    else {
      $file_object = $form_state['storage']['import_file_location'];
    }

    $snapshot_file_content = snapit_import_read_file($file_object);
    $snapshot_diff_data = snapit_import_prepare($snapshot_file_content);

    // The resulting array can contain all the implementers, but without any
    // differences/content. If $show_button stays FALSE the import button will
    // not be printed.
    $show_button = FALSE;
    $implementers = _snapit_implementers();
    foreach ($implementers as $key => $implementer) {
      if (!empty($snapshot_diff_data[$key])) {
        $show_button = TRUE;
      }
    }

    // Format all data for displaying.
    if (is_array($snapshot_diff_data)) {
      // Store the location of the import file in $form_state.
      $form_state['storage']['import_file_location'] = $file_object->filepath;

      $title_left = t('Current');
      $title_right = t('Imported snapshot data');

      $form['header'] = snapit_admin_import_header($form_state, $snapshot_file_content);
      $form['differences'] = snapit_admin_diff_table($form_state, $snapshot_diff_data, $title_left, $title_right);

      // Another block for not displaying the submit button.
      if (isset($snapshot_diff_data['blocking_detail_diffrences']) && $snapshot_diff_data['blocking_detail_diffrences'] == TRUE) {
        $show_button = FALSE;
        drupal_set_message(t('Unable to continue the import process. Please see the issued warnings and/or error messages.'), 'error');
      }
    }
    else {
      unset($form_state);
      drupal_goto('admin/build/snapit/import');
    }
  }
  elseif ($step == 3) {
    // The import results are stored in a form_state variable, after the import
    // is processed in the snapit_admin_import_submit.
    if (!empty($form_state['storage']['update_results'])) {
       $import_results = $form_state['storage']['update_results'];
    }

    // Build the page title.
    $title = t('Import') . ' - ' . t('Processed snapshot') . ' - ' . t('step @step - 3', array('@step' => $step));
    drupal_set_title($title);
    $form['#title'] = $title;

    $implementers = _snapit_implementers();

    foreach ($implementers as $key => $implementer) {
      if (!empty($import_results[$key])) {
        $form[$key] = array(
          '#type' => 'fieldset',
          '#title' => $implementer['name'],
          '#collapsible' => TRUE,
          '#collapsed' => FALSE,
          '#weight' => $weight++,
        );

        $form[$key]['#title'] = $implementer['name'];
        $form[$key]['#value'] = '<p>'. (empty($import_results[$key]) ? t('No updates were made.') :  t('Succesfully updated.') ) .'</p>';
      }
    }

    if (count($form) == 1) {
      $form['result'] = array('#value' => '<p>'.  t('No changes were made.')  .'</p>');
    }
  }

  $button_name = t('Import');

  if ($step < 2) {
    $button_name = t('Next');
  }

  if ($step < 3) {
    if ($show_button) {
      // Submit button
      $form['submit'] = array(
        '#type' => 'submit',
        '#value' => $button_name,
      );
    }
  }

  //Set enctype for uploading files
  $form['#attributes']['enctype'] = 'multipart/form-data';
  return $form;
}

/**
 * Submit callback for import form.
 *
 * @param array $form_state Current state of the import form.
 */
function snapit_admin_import_submit($form, &$form_state) {

  //If the import_data and the wanted items are selected, they can be imported
  if (!empty($form_state['storage']['import_file_location']) && $form_state['storage']['step'] == 2) {
    module_load_include('inc', 'snapit');
    module_load_include('api.inc', 'snapit');

    //Recreate snapshot_diff_data
    $snapshot_diff_data = snapit_import_prepare(snapit_import_read_file($form_state['storage']['import_file_location']), FALSE);

    $import_results = snapit_import_process($snapshot_diff_data, snapit_admin_diff_get_selected($form_state));
    $form_state['storage']['update_results'] = $import_results;
  }

  if ($form_state['storage']['step'] < 3) {
    return;
  }
}

/**
 * Header information data/form used on the snapshot import page,
 * to display information of the imported snapshot-file.
 *
 * @param array $form_state
 *   State of the form.
 * @param string $snapshot_file_content
 *   File-contents of (imported) snapshot-file.
 * @return array $form
 *   An associative array containing the structure of the compare form header
 *   (detail page).
 */
function snapit_admin_import_header($form_state, $snapshot_file_content) {

  $snapshot_import_file_data = _snapit_check_header_data($snapshot_file_content);
  $snapshot = unserialize($snapshot_import_file_data[1]);

  //Set active values
  $form['current']['title'] = array(
    '#type' => 'item',
    '#title' => t('Snapshot name'),
    '#value' => check_plain($snapshot->name),
    '#weight' => -10,
  );
  $form['current']['description'] = array(
    '#type' => 'item',
    '#title' => t('Description'),
    '#value' => check_plain($snapshot->description),
  );
  $form['current']['created'] = array(
    '#type' => 'item',
    '#title' => t('Created'),
    '#value' => format_date($snapshot->created),
  );

  $form['#theme'] = 'snapit_admin_detail_info';

  return $form;
}

/**
 * Drupal form declaration callback for the settings form.
 *
 * @return array $form An associative array containing the structure of the
 *   (system) form settings.
 */
function snapit_admin_settings() {
  module_load_include('inc', 'snapit');

  //Get and display a textarea per implementer within the drupal instalation.
  $implementers = _snapit_implementers();

  $form['general'] = array(
    '#type' => 'fieldset',
    '#title' => t('General'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );

  $form['general']['snapit_overview_limit'] = array(
    '#type' => 'select',
    '#title' => t('Overview limit'),
    '#description' => t('Number of results per page to display in the overview.'),
    '#options' => drupal_map_assoc(range(10, 200, 10)),
    '#default_value' => variable_get('snapit_overview_limit', 10),
  );

  $form['general']['snapit_key'] = array(
    '#type' => 'textfield',
    '#title' => t('Signing key'),
    '#description' => t('The signing key is used to identify similar Drupal instances of the same website. For example, you have a development version of <em>Site A</em> and a production version of <em>Site A</em>. When both instances implement the same key, they can exchange snapshots. When you try to import a snapshot of <em>Site B</em> into an instance of <em>Site A</em> a warning will be issued. A second benefit of signing snapshots is that the contents of the snapshot can be validated.'),
    '#maxlength' => 32,
    '#default_value' => snapit_unique_key(),
  );

  // Get stored exclude keys.
  $saved_settings = variable_get('snapit_exclude', array());

  // Exclusion form items.
  $form['exclude'] = array(
    '#type' => 'fieldset',
    '#title' => t('Exclusion parameters'),
    '#description' => t('Exclusion of certain parameters of the modules available for a snapshot.'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );

  $form['exclude']['snapit_exclude']['#tree'] = TRUE;

  $weight = 0;
  foreach ($implementers as $key => $implementer) {
    // Per implementer a fieldset is defined.
    $form['exclude']['snapit_exclude'][$key] = array(
      '#type' => 'fieldset',
      '#title' => $implementer['name'],
      '#description' => t('The key names below will be excluded from %implementer when creating a snapshot.', array('%implementer' => $implementer['name'])),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
      '#weight' => (!empty($implementer['weight']) ? $implementer['weight'] : $weight++),
    );

    // Textarea per implementer.
    $form['exclude']['snapit_exclude'][$key]['items_to_exclude'] = array(
      '#type' => 'textarea',
      '#parents' => array('snapit_exclude', $key),
      '#title' => t('@implementer or parameters to exclude', array('@implementer' => $implementer['name'])),
      '#description' => t('Enter item keys for exclusion, one per line.'),
      '#default_value' => isset($saved_settings[$key]) ? trim($saved_settings[$key]) : '',
    );
  }

  return system_settings_form($form);
}

/**
 * Menu callback displaying snapshot compare form.
 * If no $snapshot_compare is given, the current situation (snapshot) will be loaded.
 *
 * @param array $snapshot Previous/saved snapshot (meta-data).
 * @param array $snapshot_compare (optional) Snapshot to compare with.
 * @return array $form An associative array containing the structure of the compare form (detail page).
 */
function snapit_admin_detail($snapshot, $snapshot_compare = NULL) {
  module_load_include('inc', 'snapit');
  module_load_include('api.inc', 'snapit');

  drupal_set_title(t('Snapshot %name', array('%name' => $snapshot->name)));
  drupal_add_css(drupal_get_path('module', 'snapit') .'/snapit.css');

  // Left column; get previous/saved snapshot data
  $snapshot = snapit_load_data($snapshot);

  // Load the compare snapshot.
  if ($snapshot_compare) {
    $snapshot_compare = snapit_load($snapshot_compare);
  }

  // Right column; Load current data or other snapshot data ($snapshot_compare)
  $snapshot_compare_data = ($snapshot_compare ? snapit_load_data($snapshot_compare)->data : snapit_build_data());

  $diff = array();
  //Compare hashes, if they are equal, the comparing functionality doesn't have to be excuted.
  if (!_snapit_compare_hashes(_snapit_create_hash(serialize($snapshot->data)), _snapit_create_hash(serialize($snapshot_compare_data)))) {
    // Calculate the differences with the current data.
    $diff = snapit_compare_data($snapshot->data, $snapshot_compare_data);
  }
  else {
    drupal_set_message(t('No differences found between the snapshot and the current situation.'), 'status', FALSE);
  }

  $title_left = $snapshot->name;
  $title_right = ($snapshot_compare ? $snapshot_compare->name : t('Current'));

  $output = drupal_get_form('snapit_admin_detail_info', $snapshot, $snapshot_compare);
  $output .= drupal_get_form('snapit_admin_diff', $diff, $title_left, $title_right, $snapshot_compare);

  return $output;
}

/**
 * Header information data/form used on the snapshot compare/detail page.
 *
 * @param array $form_state
 *   State of teh form.
 * @param array $snapshot
 *   Previous/saved snapshot (meta-data).
 * @param array $snapshot_compare
 *   (optional) Snapshot to compare with.
 * @return array
 *   An associative array containing the structure of the compare form header
 *   (detail page).
 */
function snapit_admin_detail_info($form_state, $snapshot, $snapshot_compare = NULL) {
  $options = array();
  $result = db_query("SELECT sid, name, created FROM {snapit} ORDER BY created DESC");
  $options['current'] = t('Current situation');
  while ($row = db_fetch_array($result)) {
    if ($row['sid'] !== $snapshot->sid) {
      $options[$row['sid']] = $row['name'] .' - '. format_date($row['created']);
    }
  }

  $current_compare = ($snapshot_compare ? $snapshot_compare->sid : 'current');

  // Set active values
  $form['current']['title'] = array(
    '#type' => 'item',
    '#title' => t('Snapshot name'),
    '#value' => check_plain($snapshot->name),
    '#weight' => -10,
  );
  $form['current']['description'] = array(
    '#type' => 'item',
    '#title' => t('Description'),
    '#value' => check_plain($snapshot->description),
  );
  $form['current']['created'] = array(
    '#type' => 'item',
    '#title' => t('Created'),
    '#value' => format_date($snapshot->created),
  );

  $form['compare']['compare_with'] = array(
    '#type' => 'select',
    '#title' => t('Compare with'),
    '#options' => $options,
    '#default_value' => $current_compare
  );
  $form['compare']['current_snapshot'] = array(
    '#type' => 'hidden',
    '#value' => $snapshot->sid,
  );
  $form['compare']['switch_compare'] = array(
    '#type' => 'submit',
    '#value' => t('Compare')
  );
  $form['compare']['description'] = array(
    '#type' => 'item',
    '#title' => t('Description'),
    '#value' => $snapshot_compare ? check_plain($snapshot_compare->description) : t('This is the current situation of this Drupal installation.'),
    '#prefix' => '<div class="compare_desc_wrapper">',
  );
  $form['compare']['created'] = array(
    '#type' => 'item',
    '#title' => t('Created'),
    '#value' => $snapshot_compare ? format_date($snapshot_compare->created) : format_date(time()),
    '#suffix' => '</div>'
  );

  if ($snapshot_compare) {
    $form['switch'] = array(
      '#value' => l(t('<-- Switch -->'), 'admin/build/snapit/'. $snapshot_compare->sid .'/'. $snapshot->sid),
    );
  }

  $form['#theme'] = 'snapit_admin_detail_info';

  return $form;
}

/**
 * Submit callback for (detail_info) Header information data/form.
 *
 * @param array $form_state Current state of the import form.
 */
function snapit_admin_detail_info_submit($form, &$form_state) {
  if ($form_state['values']['compare_with'] != 'current') {
    $form_state['redirect'] = 'admin/build/snapit/'. $form_state['values']['current_snapshot'] .'/'. $form_state['values']['compare_with'];
  }
  else {
    $form_state['redirect'] = 'admin/build/snapit/'. $form_state['values']['current_snapshot'];
  }
}

/**
 * Setup an overview/table to see the diffrences between two
 * snapshots/situation. Also an export button/functionality is included.
 *
 * @param array $form_state
 *   Form information.
 * @param string $left_title
 *   Title used on the left-column of the overview/table.
 * @param string $right_title
 *   Title used on the right-collumn of the overview/table.
 * @param array $compare
 *   If not the current situation is loaded but another snapshot, information
 *   about this snapshot is available in this array.
 * @return array
 *   An associative array containing the structure of the compare form,
 *   differences table.
 */
function snapit_admin_diff($form_state, $diff = array(), $left_title = '', $right_title = '', $compare = NULL) {

  $form = snapit_admin_diff_table($form_state, $diff, $left_title, $right_title, $compare);

  $form['buttons'] = array(
    '#prefix' => '<div class="container-inline">',
    '#suffix' => '</div>',
    '#weight' => 100,
  );
  $form['buttons']['export'] = array(
    '#type' => 'submit',
    '#value' => t('Export'),
  );
  $form['buttons']['full_export'] = array(
    '#type' => 'checkbox',
    '#title' => t('Full export'),
    '#default_value' => 0,
  );
  return $form;
}

/**
 * Setup an overview/table to see the diffrences between two snapshots/situation.
 *
 * @param array $form_state
 *   Form status information.
 * @param string $left_title
 *   Title used on the left-collumn of the overview/table.
 * @param string $right_title
 *   Title used on the right-collumn of the overview/table.
 * @param array $compare
 *   If not the current situation is loaded but another snapshot, information
 *   about this snapshot is available in this array.
 */
function snapit_admin_diff_table($form_state, $diff = array(), $left_title = '', $right_title = '', $compare = NULL) {
  $weight = 10;
  $implementers = _snapit_implementers();

  $form['#theme'] = 'snapit_admin_diff';
  $form['#prefix'] = '<div class="compare_desc_wrapper">';
  $form['#suffix'] = '</div>';

  $form['left_title']['#value'] = $left_title;
  $form['right_title']['#value'] = $right_title;
  $form['#compare'] = ($compare) ? $compare : FALSE;

  foreach ($implementers as $key => $implementer) {
    $form[$key] = array(
      '#type' => 'fieldset',
      '#title' => $implementer['name'],
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
      '#weight' => $weight++,
    );

    if (isset($diff[$key]) && count($diff[$key])) {
      $form[$key]['#title'] .= ' ('. count($diff[$key]) .')';
      $form[$key]['#collapsed'] = FALSE;

      foreach (element_children($diff[$key]) as $child) {
        $value = $diff[$key][$child];

        $form['select'][$key]['#tree'] = TRUE;

        $form['select'][$key][$child] = array(
          '#type' => 'checkbox',
          '#title' =>  check_plain($value['#name']),
          '#default_value' => 1,
          '#return_value' => 1,
        );

        $form['left'][$key][$child]['#value'] = $value['#left'];
        $form['right'][$key][$child]['#value'] = $value['#right'];
      }
    }
    else {
      $form[$key]['#title'] .= ' (0)';
      $form[$key]['#value'] = '<p>'. t('No differences.') .'</p>';
    }
  }
  return $form;
}

/**
 * Submit callback for (detail_info) compare overview/table, export
 * functionality. Handles the export functionality.
 *
 * @param array $form Contains the form structure.
 * @param array $form_state Current state of the form.
 */
function snapit_admin_diff_submit($form, &$form_state) {
  module_load_include('api.inc', 'snapit');

  $keys = array();
  $implementers = array_keys(_snapit_implementers());
  foreach ($implementers as $key) {
    if (!empty($form_state['values'][$key])) {
      $keys[$key] = array_filter($form_state['values'][$key]);
    }
  }
  snapit_export($keys, $form['#compare'], $form_state['values']['full_export']);
}

/**
 * Helper function to filter the selected items in the compare overview/table.
 *
 * @param array $form_state
 *   Current state of the form.
 * @return array
 *   Array with key-names of the selected form-items in the compare
 *   overview/table.
 */
function snapit_admin_diff_get_selected($form_state) {
  //Get the selected parameters for import
  $keys = array();
  $implementers = array_keys(_snapit_implementers());

  foreach ($implementers as $key) {
      if (!empty($form_state['values'][$key])) {
      if ($selected_keys = array_filter($form_state['values'][$key])) {
        $keys[$key] = $selected_keys;
      }
    }
  }
  return $keys;
}

/**
 * Helper function for building the overview page query.
 * @param array $implementers Array with implementer info from hook invokation.
 * @return array Containing a SQL query
 */
function _snapit_admin_overview_query($implementers) {
  $fields = array('s.sid', 's.name', 's.description', 's.uid', 's.created', 'u.name AS account_name');
  $joins = array('INNER JOIN {snapit_data} sd ON sd.sid = s.sid', 'LEFT JOIN {users} u ON s.uid = u.uid');

  $size_fields = array('sd.size');
  $count_struct = array();

  foreach ($implementers as $key => $implementer) {
    if (!empty($implementer['table']) && !empty($implementer['field'])) {
      $alias = '_'. preg_replace('/[^a-z0-9_]+/', '_', $key);
      $joins[] = "LEFT JOIN {". $implementer['table'] ."} ". $alias ." ON s.sid = ". $alias .".". $implementer['field'];
      $fields[] = "COUNT(". $alias .".". $implementer['field'] .") AS ". $alias ."_rows";
      if (!empty($implementer['size field'])) {
        $size_field = "SUM(". $alias .".". $implementer['size field'] .")";
        $fields[] = $size_field ." AS ". $alias ."_size";
        $size_fields[] = $size_field;
      }
    }
  }
  // Add the sum of count fields as extra field.
  if (count($size_fields)) {
    $fields[] = "(". implode(" + ", $size_fields) .") AS _size_total";
  }

  $sql = "SELECT ". implode(', ', $fields) ." FROM {snapit} s ";
  $sql .= implode("\n", $joins);
  $sql .= " WHERE 1 GROUP BY s.sid";

  // Build the count query for a paged overview table.
  $sql_count = "SELECT COUNT(". $fields[0] .") FROM {snapit} s";

  return array(
    'sql' => $sql,
    'count' => $sql_count,
  );
}

/**
 * Helper function to create default 'cols' array for diff table.
 *
 * @return array
 *   Containing stylesheet classes for layout compare overview/table.
 */
function _snapit_diff_default_cols() {
  return array(array(array('class' => 'diff-marker'), array('class' => 'diff-content'), array('class' => 'diff-marker'), array('class' => 'diff-content')));
}

/**
 * Function with help contents for administrative tasks.
 *
 * @param $path string Path of/to the specific page location.
 * @return string Containg helpfull information.
 */
function _snapit_admin_help($path) {
  switch ($path) {
    case 'admin/build/snapit':
      return '<p>'. t('Here you can see an overview of all created snapshots. By default the overview is sorted by date, the most recent first.') .'</p>';

    case 'admin/build/snapit/add':
      return '<p>'. t('In this form you can take a snapshot. The snapshot will include all data provided by snapshot implementers.') .'</p>';

    case 'admin/build/snapit/import':
      return '<p>'. t('In this form you can upload a snapshot-file (.snap), select the items for the import and finally import the selected snapshot-data. If you are exchanging snapshots between different Drupal installations, make sure the signatures are equal in all installations. You can <a href="!link_settings">change the signature here</a>.', array('!link_settings' => url('admin/build/snapit/settings'))) .'</p>';

    case 'admin/build/snapit/settings':
      return '<p>'. t('In this section you can configure certain the settings for the module. You can change the signing key (signature). If you want to be able to exchange settings between Drupal installations, the signing key need to be identical. Also you could configure and decide which settings, parameters or items need to be excluded from exports and comparisons per snapshot implementer.') .'</p>';
  }
}