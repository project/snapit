<?php

/**
 * @file
 * Installation file and schema definition for
 * snapit.module
 */

/**
 * Implement hook_schema().
 * @link http://api.drupal.org/api/function/hook_schema/6
 * @link http://drupal.org/node/146843
 */
function snapit_schema() {
  $schema['snapit'] = array(
    'description' => 'The base table for snapit.module.',
    'fields' => array(
      'sid' => array(
        'description' => 'The primary identifier for a snapshot.',
        'type' => 'serial',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'name' => array(
        'description' => 'Name of the snapshot.',
        'type' => 'varchar',
        'length' => 100,
        'not null' => TRUE,
        'default' => '',
      ),
      'description' => array(
        'description' => 'A description of the snapshot.',
        'type' => 'text',
        'not null' => TRUE,
      ),
      'uid' => array(
        'description' => 'The user account used to create the snapshot. Links to {users}.uid',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'created' => array(
        'description' => 'Snapshot date and time in UNIX timestamp.',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'version' => array(
        'description' => 'Will contain the version string of the snapshot module.',
        'type' => 'varchar',
        'length' => 20,
        'not null' => TRUE,
        'default' => '',
      ),
    ),
    'indexes' => array(
      'uid' => array('uid'),
    ),
    'primary key' => array('sid'),
  );

  // Table for snapshot data that is not stored by implementers.
  $schema['snapit_data'] = array(
    'description' => 'Table for taken snapshot data. It contains a serialized snapshot.',
    'fields' => array(
      'sid' => array(
        'description' => 'The primary identifier for a snapshot, links to {snapit}.sid.',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'data' => array(
        'description' => 'Contains the serialized snapshot data.',
        'type' => 'blob',
        'size' => 'big',
        'not null' => TRUE,
      ),
      'size' => array(
        'description' => 'Size in bytes of the total snapshot data.',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
      ),
      'hash' => array(
        'description' => 'SHA1 hash of the snapshot data.',
        'type' => 'varchar',
        'length' => 40,
        'not null' => TRUE,
        'default' => '',
      ),
    ),
    'indexes' => array(
      'hash' => array('hash'),
    ),
    'primary key' => array('sid'),
  );

  return $schema;
}

/**
 * Implement hook_install().
 * @link http://api.drupal.org/api/function/hook_install/6
 */
function snapit_install() {
  // Install the schema for snapshot.
  drupal_install_schema('snapit');
}

/**
 * Implement hook_uninstall().
 * @link http://api.drupal.org/api/function/hook_uninstall/6
 */
function snapit_uninstall() {
  // Unset the declared settings. The variable 'snapshot_key' is purposely not
  // deleted, as the module might get re-enabled.
  variable_del('snapit_exclude');
  variable_del('snapit_overview_limit');

  // Uninstall the schema for snapshot.
  drupal_uninstall_schema('snapit');
}