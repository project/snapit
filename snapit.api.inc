<?php

/**
 * @file
 * Snapit API functions. These functions can be called directly to create a
 * snapshot.
 */

/**
 * Function for creating and storing a snapshot in the database.
 *
 * @param array $snapshot Snapshot meta-data, name and creation date/time.
 * @return object $snapshot The just saved snapshot.
 */
function snapit_save($snapshot) {
  module_load_include('inc', 'snapit');
  $snapshot = (object)$snapshot;

  if (empty($snapshot->version)) {
    // Retrieve the snapshot module version.
    $snapshot->version = _snapit_get_version();
  }

  $snapshot->uid = $GLOBALS['user']->uid;

  if (empty($snapshot->created)) {
    $snapshot->created = time();
  }

  $keys = NULL;
  if (!empty($snapshot->sid)) {
    $keys = 'sid';
  }

  // Write the main snapshot record.
  if (!drupal_write_record('snapit', $snapshot, $keys)) {
    return FALSE;
  }

  $snapshot->data = snapit_build_data();

  // Allow implementers to take action in storing their data.
  // Include all implementer files.
  foreach (_snapit_implementers() as $key => $implementer) {
    if (!empty($implementer['store callback'])) {
      $callback = $implementer['store callback'];
      $callback($snapshot, $implementer);
    }
  }

  // Allow other modules to store their part of the snapshot.
  drupal_alter('snapit_store', $snapshot);

  // Store the snapshot data.
  $snapshot->data = serialize($snapshot->data);
  $snapshot->size = strlen($snapshot->data);
  $snapshot->hash = _snapit_create_hash($snapshot->data);

  if (!drupal_write_record('snapit_data', $snapshot, $keys)) {
    return FALSE;
  }

  return $snapshot;
}

/**
 * Function for building data from different implementers.
 *
 * return array $snapshot Current Snapshot data
 */
function snapit_build_data() {
  module_load_include('inc', 'snapit');

  $return = array();
  // Include all implementer files.
  foreach (_snapit_implementers() as $key => $implementer) {
    _snapit_implementer_include($implementer);
    if (!empty($implementer['build callback'])) {
      $callback = $implementer['build callback'];
      $data = $callback($implementer, $key);
      if (isset($data)) {
        $return[$key] = $data;
      }
    }
  }

  // Allow other modules to build their part of the snapshot.
  drupal_alter('snapit_build', $return);

  return $return;
}

/**
 * Function for setting up the differences dataset.
 * To fullfill this compare steps and possible exclusions are executed.
 *
 * @param array $snapshot_data
 *   An array containing Snaphot data of the source or current situation.
 * @param array $build_data
 *   An array  containing Snaphot data to compare with.
 * @return array
 *   An array containing the differences between the two supplied array's with
 *   snapshot-data.
 */
function snapit_compare_data($snapshot_data, $build_data) {
  $differences = array();

  foreach (_snapit_implementers() as $key => $implementer) {

    // Include all implementer files.
    _snapit_implementer_include($implementer);

    if (!empty($implementer['compare callback'])) {
      $callback = $implementer['compare callback'];
      $diff = $callback($snapshot_data[$key], $build_data[$key], $implementer, $key);

      // Call exclude function, to exclude some parameters (if specified) from displaying
      snapit_exclude_data($diff, $implementer);

      if (isset($diff)) {
        $differences[$key] = $diff;
      }
    }
  }
  return $differences;
}

/**
 * Function for loading the snapshot data.
 *
 * @param object $snapshot Snapshot object, containing meta-data.
 * @return object $snapshot Snapshot object, snapshot-data included.
 */
function snapit_load_data($snapshot) {
  module_load_include('inc', 'snapit');

  // Load the main data.
  $data = db_result(db_query("SELECT data FROM {snapit_data} WHERE sid = %d", $snapshot->sid));

  $snapshot->data = unserialize($data);

  // Include all implementer files.
  foreach (_snapit_implementers() as $key => $implementer) {
    _snapit_implementer_include($implementer);
    if (!empty($implementer['load callback'])) {
      $callback = $implementer['load callback'];
      $data = $callback($snapshot, $implementer, $key);
      if (isset($data)) {
        $snapshot->data[$key] = $data;
      }
    }
  }
  return $snapshot;
}

/**
 * Function for comparing a key => value array.
 *
 * @param array $left
 *   Current/source snapshot data
 * @param array $right
 *   New/import snapshot data
 * @param array $implementer
 *   Implemneter info
 * @return array
 *   Array containing values and differences information of a specific
 *   key/parameter/item.
 */
function snapit_compare($left, $right, $implementer) {
  $serialized = (isset($implementer['serialized']) ? $implementer['serialized'] : FALSE);
  $return = array();

  $left['data'] = (array)$left['data'];
  $right['data'] = (array)$right['data'];

  // Build the keys from left and right arrays.
  $keys = array_unique(array_merge(array_keys($left['data']), array_keys($right['data'])));

  foreach ($keys as $key) {
    if (isset($left['data'][$key]) && !isset($right['data'][$key])) {
      $left_data = ($serialized ? unserialize($left['data'][$key]) : $left['data'][$key]);
      $return[$key] = array(
        '#type' => SNAPIT_REMOVED,
        '#name' => $key,
        '#left' => $left_data,
        '#right' => NULL,
      );
    }
    elseif (isset($right['data'][$key]) && !isset($left['data'][$key])) {
      $right_data = ($serialized ? unserialize($right['data'][$key]) : $right['data'][$key]);
      $return[$key] = array(
        '#type' => SNAPIT_ADDED,
        '#name' => $key,
        '#left' => NULL,
        '#right' => $right_data,
      );
    }
    elseif ($left['data'][$key] !== $right['data'][$key]) {
      $left_data = ($serialized ? unserialize($left['data'][$key]) : $left['data'][$key]);
      $right_data = ($serialized ? unserialize($right['data'][$key]) : $right['data'][$key]);

      $return[$key] = array(
        '#type' => SNAPIT_MODIFIED,
        '#name' => $key,
        '#left' => $left_data,
        '#right' => $right_data,
      );
    }
  }
  return $return;
}

/**
 * Export functionality.
 *
 * Creates a file with either all data or just selected differences.
 *
 * @param $keys
 *   Optional multidimensional array with selected keys to export.
 * @param $compare
 *   A snapshot object to use for building the diff export. If left is empty,
 *   the current configuration is used.
 * @param $full
 *   Flag for requesting either a full export or differences only.
 */
function snapit_export($keys, $compare = NULL, $full = FALSE) {
  // Load the data of which values should be used
  $snapshot_compare = ($compare ? snapit_load_data($compare)->data : snapit_build_data());
  $export = array();

  if ($full) {
    // Full export.

    // Exclusion of paramters per implementer.
    $implementers = _snapit_implementers();
    foreach ($implementers as $implementer) {
      snapit_exclude_data($snapshot_compare[$implementer['key']]['data'], $implementer);
    }

    foreach ($snapshot_compare as $implementer => $data) {
      $export[$implementer] = array();
      $items = array_keys($data['data']);

      foreach ($items as $key) {
        $export[$implementer]['version'] = $snapshot_compare[$implementer]['version'];
        $export[$implementer]['data'][$key] = NULL;
        if (isset($snapshot_compare[$implementer]['data'][$key])) {
          $export[$implementer]['data'][$key] = $snapshot_compare[$implementer]['data'][$key];
        }
      }
    }
  }
  else {
    // Export differences only.
    foreach ($snapshot_compare as $implementer => $data) {
      $export[$implementer] = array();
      if (isset($keys[$implementer])) {
        foreach (array_keys($keys[$implementer]) as $key) {
          $export[$implementer]['version'] = $snapshot_compare[$implementer]['version'];
          $export[$implementer]['data'][$key] = NULL;
          if (isset($snapshot_compare[$implementer]['data'][$key])) {
            $export[$implementer]['data'][$key] = $snapshot_compare[$implementer]['data'][$key];
          }
        }
      }
    }
  }

  // Snapshot meta-data (name, description, version, etc.).
  if (empty($compare->name)) {
    global $base_url;
    $compare->name = t('@site_name snapshot export', array('@site_name' => variable_get('site_name', 'Drupal')));
    $compare->description = t('A snapshot export of @site_name (@host).', array(
      '@site_name' => variable_get('site_name', 'Drupal'),
      '@host' =>  $base_url,
    ));
  }

  $compare->version = _snapit_get_version();
  $compare->created = time();

  $compare->data = $export;
  // A hash code is generated which will be included in the export file.
  // note: We need to create the hash of the data, because this dataset is used
  // elsewhere also and used for hash-compare actions. Besides the hash-code,
  // the file-content is made up here.
  $file_content = _snapit_set_header_data(serialize($compare->data)) . serialize($compare);

  $file_name = 'snapshot-' . ($full ? 'full' : 'differences') .'-'. format_date(time(), 'custom', 'Ymd-H:i:s') .'-'. (!empty($compare->sid) ? $compare->sid : '0') .'.snap';

  header('Content-Type: application/octet-stream');
  header('Content-Disposition: attachment; filename="' . $file_name . '"');

  // @todo Check if binary is the right way to go.
  header('Content-Transfer-Encoding: binary');
  print $file_content;
  exit;
}

/**
 * This function is a part of the import functionality and handles the (temporary) saving
 * of a just uploaded snapshot file.
 *
 * @todo alternative file reading (file_get_contents) for commandline importing.
 *
 * @return mixed
 *   object If saving succeeded file object is returned.
 *   bool FALSE If no uploaded file is found.
 */
function snapit_import_save_file() {
  if (file_exists($_FILES['files']['tmp_name']['import_upload'])) {

    // Validators, at this moment we only validate on extension.
    $validators = array(
      '_snapit_validate_file' => array('snap'),
    );

    if ($file = file_save_upload('import_upload', $validators)) {
      return $file;
    }
    else {
      drupal_set_message(t("The upload could not be completed."), 'error');
    }
  }
  else {
    drupal_set_message(t("No file was selected."), 'warning');
  }
  return FALSE;
}

/**
 * Validator function, used to rename the snapshot file. For safety reasons we
 * do not allow user 1 to upload a file with a different extension.
 *
 * @param object $file Drupal file object.
 * @param array $extensions Array containing allowed extensions.
 * @return array Returns an array of errors if occurred.
 */
function _snapit_validate_file($file, $extensions) {
  $errors = array();
  $regex = '/\.('. ereg_replace(' +', '|', preg_quote($extensions)) .')$/i';
  if (!preg_match($regex, $file->filename)) {
    $errors[] = t('Only files with the following extensions are allowed: %files-allowed.', array('%files-allowed' => $extensions));
  }
  return $errors;
}

/**
 * Read the file contents, a file object or (complete) file-path must be
 * provided.
 *
 * @param mixed $file
 *   Drupal File Object or (string) complete filepath+filename.
 * @return string
 *  Returns file contents.
 */
function snapit_import_read_file($file) {
  if (is_object($file)) {
    if ((!$file = $file->filepath)) {
      return FALSE;
    }
  }
  elseif (!file_exists($file)) {
    return FALSE;
  }
  if (!empty($file)) {
    return file_get_contents($file);
  }
}

/**
 * Prepares a serialized snapshot object to be used in an import sequence.
 *
 * @param string $contents
 *   String contents, usually a serialized object with snapshot data for all
 *   implementers.
 *
 * @return array
 *  Returns an array containing all differences.
 */
function snapit_import_prepare($contents, $display_msgs = TRUE) {
  // Check the checksum/hash.
  $snapshot_import_file_data = _snapit_check_header_data($contents);

  if (is_array($snapshot_import_file_data)) {
    //sha1 hash from snapshot file
    $signature = $snapshot_import_file_data[0];

    $snapshot_import_file_object = unserialize($snapshot_import_file_data[1]);

    // Prepare/Unpack the snapshot data.
    $snapshot_import_file_data = $snapshot_import_file_object->data;

    // Right column; Load current data or other snapshot data ($snapshot_compare)
    $snapshot_current_data = snapit_build_data();

    // Check the snapshot module version.
    if ($snapshot_import_file_object->version != _snapit_get_version()) {
      drupal_set_message(t("The Snapit version mentioned in the snapshot-file doesn't match the installed version (@snapshot_version) in the this Drupal installation.", array('@snapshot_version' => _snapshot_get_version())), 'warning');
    }

    // Compare hashes
    if (_snapit_compare_hashes(_snapit_create_hash(serialize($snapshot_current_data)), $signature)) {
      drupal_set_message(t('No diffrences or updates found in snapshot-file compared to the current Drupal installation.'));
      return FALSE;
    }

    // Allow all implementers to perform their version checks.
    $implementers = _snapit_implementers();

    foreach ($implementers as $key => $implementer) {
      // Check the versions of the implementer of snapshot data and the current
      // installed one.
      if (!empty($snapshot_import_file_data[$key])) {
        if (!_snapit_check_implementer_version($snapshot_import_file_data[$key], $implementer)) {
          //Version current implementer doesn't match the version mentioned in the snapshot file.
          drupal_set_message(t("The version of the @implementer installed and mentioned in the snapshot do not match.", array('@implementer' => $implementer['name'])), 'error');
          $error = TRUE;
        }

        // If available process an additional check by the implementer.
        if (!empty($implementer['check callback'])) {
          $check_callback = $implementer['check callback'];
          if (!$check_callback($snapshot_import_file_data[$key], $implementer)) {
            // Throw an error.
            drupal_set_message(t("The data validation for @implementer failed.", array('@implementer' => $implementer['name'])), 'error');
            $error = TRUE;
          }
        }
      }
    }
    // If the check above failed.
    if ($error) {
      return FALSE;
    }

    // Merge the imported snapshot data with the current situation data.
    // Merge the current data with the imported data and save this in the
    // updated array. This is necessary for a correct compare, when we've got
    // imported a differences export file, this only contains a selection of
    // snapshot parameters. So if we would compare this set, there's a lot to
    // update...
    $snapshot_compare_data = snapit_update_array_merge($snapshot_current_data, $snapshot_import_file_data);

    // Perform the regular compare callbacks via implementers. Calculate the
    // differences with the current data.
    $diff = snapit_compare_data($snapshot_current_data, $snapshot_compare_data);

    // Perfom details callback, if available get some additional details of
    // (sub) items of the implementer.
    if ($display_msgs) {
      foreach ($implementers as $key => $implementer) {
        _snapit_implementer_include($implementer);

        if (!empty($implementer['details callback'])) {
          $check_callback = $implementer['details callback'];
          if ($blocking_detail_differences = $check_callback($diff[$key], $implementer)) {
            $diff['blocking_detail_diffrences'] = $blocking_detail_differences;
          }
        }
      }
    }

    // Unset some variables.
    unset($snapshot_import_file_data);
    unset($snapshot_current_data);

    // Return the compared data.
    return $diff;
  }
  else {
    drupal_set_message(t("Unable to process uploaded file. Possibly you've uploaded a wrong file or a snapshot-file with a wrong signing key (signature)."), 'error');
    return FALSE;
  }
}

/**
 * Process the (selected) data, provided for a snapshot import.
 * If the user have made a selection of items he/she wants to import,
 * all other items will be excluded from importing.
 *
 * @param array $import_data
 *   All the data which could be imported per implementer.
 * @param array
 *   Returns a selection of items specified by the user.
 */
function snapit_import_process($import_data, $selected_keys = NULL) {
  $implementers = _snapit_implementers();

  // Unset vars from the import_data, if they are not selected.
  if (is_array($selected_keys) && is_array($import_data)) {
    foreach (array_keys($implementers) as $implementer) {
      if (isset($selected_keys[$implementer])) {
        foreach ($import_data[$implementer] as $key => $values) {
          if (!key_exists($key, $selected_keys[$implementer])) {
            unset($import_data[$implementer][$key]);
          }
        }
      }
      else {
        unset($import_data[$implementer]);
      }
    }
  }

  // Process the import by using the specific implementer callback function.
  if (is_array($import_data)) {
    $update_results = array();

    if (!empty($import_data)) {
      //First back_up the current situation.
      //note: we could make this optional.
      $snapshot = array(
        'name' => t('Backup: @date', array('@date' => format_date(time()))),
        'description' => t('Automatically created snapshot, backup of the situation before importing a snapshot.'),
      );
      if ($snapshot_saved = snapit_save($snapshot)) {
        drupal_set_message(t('Successfully created snapshot %name.', array('%name' => $snapshot_saved->name)));
      }
      else {
        // If the back-up isn't saved, we won't proceed.
        drupal_set_message(t('An error occurred while saving the backup snapshot. Please check the error logs.'), 'error');
        return FALSE;
      }

      // The final import processing.
      foreach ($implementers as $key => $implementer) {
        _snapit_implementer_include($implementer);
        if (!empty($import_data[$key])) {
          if ($implementer['import callback']) {
            // Call each implementer's update function.
            $import_callback = $implementer['import callback'];
            $update_results[$key] = $import_callback($import_data[$key]);
          }
        }
      }
    }

    // Flush all caches. The implementer update functions should clear their
    // own caches.
    drupal_flush_all_caches();

    return $update_results;
  }
  else {
    drupal_set_message(t('An error occured while updating the data.'), 'error');
  }
}