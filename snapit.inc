<?php

/**
 * @file
 * Contains snapshot helper functions.
 */

/**
 * Function for retrieving information about all modules that implement
 * snapshot functionality.
 *
 * The hook implementers are expected to return an array containing a name and
 * description for their implementation. Besides that implementers are expected
 * to provide information about their database table structure. This allows the
 * snapshot module to extend queries to automatically include/join information
 * from those tables.
 *
 * @param bool $refresh
 *   Optional refresh parameter which will explicitly refresh the information
 *   instead of using the static variable.
 *
 * @return array
 *   Returns array containing all implementers information.
 */
function _snapit_implementers($refresh = FALSE) {
  static $implementers;
  if (!isset($implementers) || $refresh) {

    _snapit_implementers_include();

    $weight = 0;
    foreach (module_implements('snapit_info') as $module) {
      $data = module_invoke($module, 'snapit_info');
      if (isset($data) && is_array($data)) {
        foreach ($data as $delta => $info) {
          $key = $module .'-'. $delta;
          $info['key'] = $key;
          $info['module'] = $module;
          $info['delta'] = $delta;

          $info += array(
            'serialized' => FALSE,
          );

          // if no weight is set by the implementer, a weight will be set.
          if (!isset($info['weight'])) {
            $info['weight'] = $weight++;
          }
          $implementers[$key] = $info;
        }
      }
    }
    // Sort the implementers on their weight.
    uasort($implementers, "_snapit_implementers_sort");
  }
  return $implementers;
}

/**
 * Sort helper function for the implementers. Implementers can set a 'weight'
 * key in their info array.
 */
function _snapit_implementers_sort($a, $b) {
  if ($a['weight'] == $b['weight']) {
    return 0;
  }
  return ($a['weight'] < $b['weight']) ? -1 : 1;
}

/**
 * Function for reading through the 'on behalf' modules folder and including
 * all necessary hook implementations.
 */
function _snapit_implementers_include() {
  $path = drupal_get_path('module', 'snapit') .'/modules';

  // Loop through the implementers directory and include.
  if (is_dir($path) && $handle = opendir($path)) {
    while (FALSE !== ($file = readdir($handle))) {
      if (substr($file, 0, 7) == 'snapit.' && substr($file, strrpos($file, '.') + 1) == 'inc' && module_exists(substr($file, 7, strrpos($file, '.') - 7))) {
        include_once('./'. $path .'/'. $file);
      }
    }
    closedir($handle);
  }
}

/**
 * Function for including the defined snapshot implementer include file. *
 * @return bool TRUE When the include is a succes, returns FALSE when the file
 *   couldn't be found or if no implementer include is needed.
 */
function _snapit_implementer_include($definition) {
  if (!empty($definition['file'])) {
    $path = (!empty($definition['file path']) ? $definition['file path'] : drupal_get_path('module', $definition['module']));
    $file = './'. $path .'/'. $definition['file'];
    if (is_file($file)) {
      require_once $file;
      return TRUE;
    }
  }
  return FALSE;
}

/**
 * Get the current Snapshot version (incl. system version).
 * @return string The current version of the Snapshot Module and Drupal Core.
 */
function _snapit_get_version() {
  $module = snapit_module_info('snapit');
  $system = snapit_module_info('system');
  return trim($module->info['version'] .'/'. $system->info['version']);
}

/**
 * Check the version of the implementer in the snapshot file with it's current
 * version.
 *
 * @param array $data Snapshot data (imported).
 * @param array $implementer Implementer info
 * @return bool TRUE if the versions match.
 */
function _snapit_check_implementer_version($data, $implementer) {
  $module_info = snapit_module_info($implementer['module']);
  if ($data['version'] == $module_info->info['version']) {
    return TRUE;
  }
  return FALSE;
}

/**
 * Helper function for retrieving a module's version.
 *
 * @return array
 *   An array containing module information (name, version, status, etc.).
 */
function snapit_module_info($name, $refresh = FALSE) {
  return snapit_system_info($name, 'module', $refresh);
}

/**
 * Helper function for retrieving a theme's version and other information.
 *
 * @return array
 *   An array containing theme information (name, version, status, etc.).
 */
function snapit_theme_info($name, $refresh = FALSE) {
  return snapit_system_info($name, 'theme', $refresh);
}

/**
 * Helper function for retrieving information from the system table.
 * @return array
 *   An array containing information of an item available in the system table.
 */
function snapit_system_info($name, $type, $refresh = FALSE) {
  static $info;
  if (!isset($info[$type][$name]) || $refresh) {
    $info[$type][$name] = FALSE;
    if ($record = db_fetch_object(db_query("SELECT name, status, schema_version, weight, info FROM {system} WHERE type = '%s' AND name = '%s'", $type, $name))) {
      $record->info = @unserialize($record->info);
      $info[$type][$name] = $record;
    }
  }
  return $info[$type][$name];
}

/**
 * Helper function for calling the diff engine.
 *
 * @param array $left
 *   An array containing the value(s) of an item in the current/source snapshot.
 * @param array $right
 *   An array containing the new value(s) of an item in the compare snapshot.
 * @return array
 *   An array with formated data for displaying te compare/differences results.
 */
function snapit_diff($left, $right) {
  include_once(drupal_get_path('module', 'snapit') .'/includes/diff_engine.inc');

  $left = _snapit_value_prepare($left);
  $right = _snapit_value_prepare($right);

  $lines_left = preg_split("/(\r\n|\r|\n)/", $left);
  $lines_right = preg_split("/(\r\n|\r|\n)/", $right);

  $rows = array();
  $keys = array_merge(array_keys($lines_left), array_keys($lines_right));

  $diff = new Diff($lines_left, $lines_right);
  $formatter = new DrupalDiffFormatter();
  $formatter->show_header = FALSE;
  $diff_rows = $formatter->format($diff);

  return $diff_rows;
}

/**
 * Prepare every single value for comparison.
 * @return string
 *   If the input value is an array or is NULL the value is converted to a
 *   string.
 */
function _snapit_value_prepare($value) {
  if (is_object($value) || is_array($value)) {
    return rtrim(print_r($value, TRUE));
  }
  elseif (is_integer($value)) {
    return '(int) '. $value;
  }
  elseif (is_null($value)) {
    return 'NULL';
  }
  elseif (is_string($value) && $value == 'NULL') {
    return '(string) "NULL"';
  }
  elseif (is_string($value) && $value == 'FALSE') {
    return '(string) "FALSE"';
  }
  elseif (is_string($value) && $value == 'TRUE') {
    return '(string) "TRUE"';
  }
  elseif (is_bool($value)) {
    return '(bool) ' . ($value === TRUE ? 'TRUE' : 'FALSE');
  }
  return $value;
}

/**
 * Exclude parameters from the snapshot data based on the exclusion
 * settings/configuration or if a hook is specified for the specific module,
 * this will be used for exclusion. The function returns an array with possible
 * exclusion of some parameters.
 *
 * @param $data array The snapshot data
 * @param $implementer string the specific implementer (module-delta).
 * @param $module string optional module name. Used to look for a hook.
 * @return $data array The processed snapshot data.
 */
function snapit_exclude_data(&$diff, $implementer) {
  // Exclude keys from the variable_get('snapit_exclude', array())
  $exclude = variable_get('snapit_exclude', array());
  if (!empty($exclude[$implementer['key']])) {
    if ($exclude = preg_split("/(\r\n|\r|\n)/", trim($exclude[$implementer['key']]))) {
      foreach ($exclude as $key) {
        unset($diff[$key]);
      }
    }
  }

  // Allow other modules to alter the built diff.
  drupal_alter('snapit_compare', $diff, $implementer);
}

/**
 * Merge two arrays and update or add values, which are provided by the second
 * parameter. Resulting in a merged array, but with all the differences
 * compared to the existing situation.
 *
 * @param array $existing
 *   Master array/Current situation which needs to be updated.
 * @param array $updates Contains the updates/changes and is structured the
 *   same way as the $existing
 * @return array
 *   Updated and merged array.
 */
function snapit_update_array_merge($existing, $updates) {
  $new = $existing;
  $keys = array_keys($updates);

 foreach ($keys as $implementer) {
    $data = $updates[$implementer]['data'];

    if (is_array($data)) {
      // Data is in key => value format, keys are leading.
      foreach ($data as $key => $value) {
        $new[$implementer]['data'][$key] = $value;
      }
    }
    elseif (!empty($data)) {
      // Just overwrite the value.
      $new[$implementer]['data'] = $data;
    }
  }
  return $new;
}

/**
 * Create a sha1 hash of the content or other data.
 * This hashcode is used for checks and compare actions of the snapshot data.
 *
 * @param string $content snapshot-data or another value.
 * @retun string sha1_hash The sha1 hash based on the input data.
 */
function _snapit_create_hash($content) {
  // Besides the content, the unique snapit_key is added.
  return sha1(snapit_unique_key() . $content);
}

/**
 * Snapshot file, header data function. Returns a hash code partly based on the
 * content of the snapshot file.
 *
 * @param string $content (serialized) Text/content of a snapshot.
 * @return string snapshot_header_hash A string containing a hash of the content.
 */
function _snapit_set_header_data($content) {
  return _snapit_create_hash($content) ."\n";
}

/**
 * Check the header data, does the hashcode match with the snapshot data in the
 * file.
 *
 * @param string $file_data
 *   The (imported) file_data.
 * @return array
 *   File content, pos; [0]:header-data, pos; [1]:snapshot-data.
 */
function _snapit_check_header_data($file_data) {
  // Header data.
  $signature = trim(substr($file_data, 0, 40));

  // Snapshot data.
  $content = trim(substr($file_data, 40));

  // The hash code in the file, is generated based on the actual data.
  $file_content_object = unserialize($content);

  if (_snapit_compare_hashes($signature, _snapit_create_hash(serialize($file_content_object->data)))) {
    return array($signature, $content);
  }
  return FALSE;
}

/**
 * Compare two hashes (sha1) with eachother.
 *
 * @param string $source_hash
 *   SHA1 hash, default this string is already encoded.
 * @param string $compare_hash
 *   SHA1 hash, default this string is already encoded.
 * @param bool
 *   Returns TRUE when both hashes are identical.
 */
function _snapit_compare_hashes($source, $compare) {
  if ($source === $compare) {
    return TRUE;
  }
  return FALSE;
}